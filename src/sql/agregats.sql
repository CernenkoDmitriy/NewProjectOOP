USE academy;

-- Количество преподавателей
SELECT count(teachers.id) as amount_teachers
from teachers;

-- Количество студентов в определенной группе

SELECT count(student_group.id_student) as amount_students
from student_group
join groups
on student_group.id_group = groups.id
where groups.group_name = 'MT-2005-07';

-- Количество студентов определенной формы обучения
SELECT count(student_group.id_student) as amount_students
from student_group
join groups
on student_group.id_group = groups.id
join forms
on groups.id_form = forms.id
where forms.form_name = 'Дневная';

-- Средняя оценка всех студентов определенной группы

select avg(marks.mark) as avg_mark
from marks
join groups
on marks.id_group = groups.id
where groups.group_name = 'ZV-2004-08';

-- Форма обучения, с максимальным количеством студентов

select t1.form_name
from (
select  forms.form_name, count(student_group.id_group) 
from forms
join groups
on groups.id_form = forms.id
join student_group
on student_group.id_group = groups.id
group by  forms.id
limit 1) as t1;