USE academy;

-- получить список всех преподавателей (имя, фамилия, id)
SELECT DISTINCT passport.first_name, passport.lastname, passport.id
FROM passport
JOIN teachers 
ON passport.id = teachers.id_passport;

-- олучить список преподавателей (имя, фамилия, id), читающих определенный предмет (известно название предмета)
SELECT DISTINCT passport.first_name, passport.lastname, passport.id
FROM passport
JOIN teachers 
ON passport.id = teachers.id_passport
JOIN subject_teacher
ON teachers.id = subject_teacher.id_teacher
JOIN subjects
ON subject_teacher.id_subject = subjects.id
WHERE subjects_name = 'IT';

-- получить список студентов (имя, фамилия, id) определенной форму обучения (форма задана именем) (Дневная)
SELECT DISTINCT passport.first_name, passport.lastname, passport.id
FROM passport
JOIN student 
ON passport.id = student.id_passport
JOIN student_group
ON student_group.id_student = student.id
JOIN groups
ON student_group.id_group = groups.id
JOIN forms
ON groups.id_form = forms.id
WHERE forms.form_name = 'Дневная';

-- получить список студентов-двоечников (есть одна двойка и больше)(имя, фамилия, id) у меня нет двоечников, есть троечникик :)
SELECT DISTINCT passport.first_name, passport.lastname, passport.id
FROM passport
JOIN student 
ON passport.id = student.id_passport
JOIN marks
ON student.id = marks.id_student
WHERE marks.mark = '3';

