CREATE database CarNumbers;
USE CarNumbers;

CREATE TABLE CarNumbers (
	id INT NOT NULL AUTO_INCREMENT,
    car_number VARCHAR(64) NOT NULL,
    PRIMARY KEY (id)
);
    
CREATE TABLE car (
	id INT NOT NULL AUTO_INCREMENT,
    mark VARCHAR (64) NOT NULL DEFAULT 'Uncnown',    
    model VARCHAR (64) NOT NULL DEFAULT 'Uncnown',    
	release_date INT,
    car_number_id INT,
    PRIMARY KEY (id),
    FOREIGN KEY (car_number_id) REFERENCES CarNumbers (id)
    ON DELETE SET NULL ON UPDATE CASCADE
);