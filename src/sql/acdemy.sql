CREATE DATABASE academy;
USE academy;

CREATE TABLE Passport (
id INT NOT NULL AUTO_INCREMENT,
first_name VARCHAR(64) NOT NULL,
lastname VARCHAR(64) NOT NULL,
PRIMARY KEY (id)
);

CREATE TABLE Student (
	id INT NOT NULL AUTO_INCREMENT,
    id_passport INT,
    _date DATE,
    PRIMARY KEY (id),
    FOREIGN KEY (id_passport) REFERENCES Passport (id)
    ON DELETE SET NULL ON UPDATE CASCADE
);

CREATE TABLE Forms (
	id INT NOT NULL AUTO_INCREMENT,
	form_name VARCHAR(64) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE Groups (
	id INT NOT NULL AUTO_INCREMENT,
	group_name VARCHAR(64) NOT NULL,
	id_form INT,
	PRIMARY KEY (id),
	FOREIGN KEY (id_form) REFERENCES Forms (id)
    ON DELETE SET NULL ON UPDATE CASCADE
);

CREATE TABLE Student_group (
	id_student INT,
    id_group INT,
    FOREIGN KEY (id_student) REFERENCES Student (id),
    FOREIGN KEY (id_group) REFERENCES Groups (id)
);

CREATE TABLE Teachers (
	id INT NOT NULL AUTO_INCREMENT,
    id_passport INT,
    PRIMARY KEY (id),
    FOREIGN KEY (id_passport) REFERENCES Passport (id)
    ON DELETE SET NULL ON UPDATE CASCADE
);

CREATE TABLE Subjects (
	id INT NOT NULL AUTO_INCREMENT,
    subjects_name VARCHAR(64) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE Subject_teacher (
	id_subject INT,
    id_teacher INT,
    FOREIGN KEY (id_subject) REFERENCES Subjects (id),
    FOREIGN KEY (id_teacher) REFERENCES Teachers (id)
);

CREATE TABLE Marks (
	id INT NOT NULL AUTO_INCREMENT,
    id_student INT,
    id_group INT,
    id_subject INT,
    mark INT,
    _date DATE,
    id_teacher INT,
    PRIMARY KEY (id),
    FOREIGN KEY (id_subject) REFERENCES Subjects (id),
    FOREIGN KEY (id_teacher) REFERENCES Teachers (id),
	FOREIGN KEY (id_student) REFERENCES Student (id),
    FOREIGN KEY (id_group) REFERENCES Groups (id)
);