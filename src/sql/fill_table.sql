INSERT INTO passport  (first_name, lastname)
VALUES	('Vasya', 'Pupkin');
    
INSERT INTO passport  (first_name, lastname)
VALUES	('Petay', 'Durov');

INSERT INTO passport  (first_name, lastname)
VALUES	('Ira', 'Pugacheva');
    
INSERT INTO passport  (first_name, lastname)
VALUES	('Ura', 'Vybluy');

INSERT INTO passport  (first_name, lastname)
VALUES	('Dima', 'Potapenko');

INSERT INTO passport  (first_name, lastname)
VALUES	('Vova', 'Lastochkin');

INSERT INTO passport  (first_name, lastname)
VALUES	('Olay', 'Polaykova');

INSERT INTO passport  (first_name, lastname)
VALUES	('Jora', 'Bush');

INSERT INTO passport  (first_name, lastname)
VALUES	('Chack', 'Noris');

INSERT INTO passport  (first_name, lastname)
VALUES	('Boris', 'Filatov');

INSERT INTO passport  (first_name, lastname)
VALUES	('Arnold', 'Bobik');

INSERT INTO passport  (first_name, lastname)
VALUES	('Kristina', 'Agilera');

INSERT INTO passport  (first_name, lastname)
VALUES	('Lera', 'Kurina');

INSERT INTO passport  (first_name, lastname)
VALUES	('Anton', 'Kupovoy');

INSERT INTO passport  (first_name, lastname)
VALUES	('Petay', 'Paytochkin');

INSERT INTO passport  (first_name, lastname)
VALUES	('Nester', 'Kakoyto');

INSERT INTO passport  (first_name, lastname)
VALUES	('Alfred', 'Gir');
    
INSERT INTO student  (id_passport, _date)
VALUES	('1', '2017-08-01');

INSERT INTO student  (id_passport, _date)
VALUES	('2', '2017-08-05');

INSERT INTO student  (id_passport, _date)
VALUES	('3', '2017-07-07');

INSERT INTO student  (id_passport, _date)
VALUES	('4', '2017-08-14');

INSERT INTO student  (id_passport, _date)
VALUES	('5', '2017-06-07');

INSERT INTO student  (id_passport, _date)
VALUES	('6', '2017-05-21');

INSERT INTO student  (id_passport, _date)
VALUES	('7', '2017-06-11');

INSERT INTO student  (id_passport, _date)
VALUES	('8', '2017-04-25');

INSERT INTO student  (id_passport, _date)
VALUES	('9', '2017-08-28');

INSERT INTO student  (id_passport, _date)
VALUES	('10', '2017-04-13');

INSERT INTO student  (id_passport, _date)
VALUES	('11', '2017-05-18');

INSERT INTO student  (id_passport, _date)
VALUES	('12', '2017-06-22');


INSERT INTO teachers  (id_passport)
VALUES	('13');

INSERT INTO teachers  (id_passport)
VALUES	('14');

INSERT INTO teachers  (id_passport)
VALUES	('15');

INSERT INTO forms  (form_name)
VALUES	('Дневная');

INSERT INTO forms  (form_name)
VALUES	('Заочная');

INSERT INTO groups  (group_name, id_form)
VALUES	('MT-2005-07', '1');

INSERT INTO groups  (group_name, id_form)
VALUES	('ZV-2004-08', '1');

INSERT INTO groups  (group_name, id_form)
VALUES	('KL-2005-06', '2');

INSERT INTO student_group  (id_student, id_group)
VALUES	('1', '2');

INSERT INTO student_group  (id_student, id_group)
VALUES	('2', '2');

INSERT INTO student_group  (id_student, id_group)
VALUES	('3', '2');

INSERT INTO student_group  (id_student, id_group)
VALUES	('4', '2');

INSERT INTO student_group  (id_student, id_group)
VALUES	('5', '2');

INSERT INTO student_group  (id_student, id_group)
VALUES	('6', '1');

INSERT INTO student_group  (id_student, id_group)
VALUES	('7', '1');
INSERT INTO student_group  (id_student, id_group)
VALUES	('8', '1');

INSERT INTO student_group  (id_student, id_group)
VALUES	('9', '3');

INSERT INTO student_group  (id_student, id_group)
VALUES	('10', '3');

INSERT INTO student_group  (id_student, id_group)
VALUES	('11', '1');

INSERT INTO student_group  (id_student, id_group)
VALUES	('12', '2');

INSERT INTO subjects (subjects_name)
VALUES ('history');

INSERT INTO subjects (subjects_name)
VALUES ('IT');

INSERT INTO subjects (subjects_name)
VALUES ('mathematics');

INSERT INTO subject_teacher (id_subject, id_teacher)
VALUES ('1', '6');

INSERT INTO subject_teacher (id_subject, id_teacher)
VALUES ('2', '7');

INSERT INTO subject_teacher (id_subject, id_teacher)
VALUES ('3', '8');


INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('1', '2', '1', '5', '2017-10-5', '6');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('1', '2', '2', '4', '2017-10-15', '7');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('2', '2', '1', '3', '2017-10-9', '6');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('2', '2', '2', '4', '2017-11-01', '7');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('2', '2', '3', '4', '2017-10-19', '8');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('3', '2', '1', '4', '2017-10-9', '6');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('3', '2', '1', '5', '2017-10-29', '6');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('3', '2', '3', '5', '2017-11-19', '8');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('4', '2', '1', '5', '2017-10-8', '6');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('2', '2', '2', '4', '2017-10-9', '7');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('5', '2', '1', '5', '2017-09-21', '6');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('5', '2', '3', '5', '2017-09-21', '8');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('5', '2', '1', '3', '2017-09-15', '6');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('6', '1', '1', '5', '2017-11-21', '6');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('6', '1', '2', '4', '2017-11-28', '7');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('6', '1', '3', '5', '2017-11-11', '8');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('7', '1', '1', '3', '2017-11-24', '6');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('7', '1', '3', '3', '2017-11-28', '8');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('8', '1', '1', '4', '2017-11-15', '6');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('8', '1', '1', '5', '2017-11-17', '6');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('8', '1', '2', '5', '2017-11-15', '7');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('9', '3', '1', '4', '2017-11-15', '6');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('9', '3', '2', '5', '2017-11-18', '7');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('9', '3', '3', '5', '2017-11-25', '8');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('10', '3', '1', '5', '2017-11-15', '6');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('10', '3', '2', '4', '2017-11-17', '7');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('10', '3', '3', '4', '2017-11-18', '8');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('11', '1', '1', '5', '2017-11-11', '6');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('11', '1', '2', '4', '2017-11-12', '7');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('11', '1', '3', '5', '2017-11-14', '8');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('12', '2', '1', '5', '2017-11-17', '6');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('12', '2', '2', '3', '2017-11-27', '7');

INSERT INTO marks (id_student, id_group, id_subject, mark, _date, id_teacher)
VALUES ('12', '2', '3', '4', '2017-11-27', '8');

    select *
    from marks;
    

