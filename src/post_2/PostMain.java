package post_2;

import post_2.entities.Post2;
import post_2.entities.Postman;
import post_2.entities.Sender;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PostMain {
    static Post2 myPost2 = new Post2(20);
    static boolean isThreadActive = true;

    public static void main(String[] args) throws InterruptedException {

        ExecutorService myThread = Executors.newCachedThreadPool();
        myThread.execute(new Sender(myPost2, "1"));
        myThread.execute(new Sender(myPost2, "2"));
        myThread.execute(new Sender(myPost2, "3"));
        myThread.execute(new Sender(myPost2, "4"));

        myThread.execute(new Postman(myPost2, "1"));
        myThread.execute(new Postman(myPost2, "2"));
        myThread.execute(new Postman(myPost2, "3"));


        Thread.sleep(5000);
        myThread.shutdownNow();
        isThreadActive = false;

        System.out.println("Sent letters - " + myPost2.getAmountSentMails());
        System.out.println("Letters in post - " + myPost2.getSize());
        System.out.println("Get letters by postman - " + myPost2.getAmountTakingMails());
        Thread.sleep(1000);
        System.out.println("Sent letters - " + myPost2.getAmountSentMails());
        System.out.println("Letters in post - " + myPost2.getSize());
        System.out.println("Get letters by postman - " + myPost2.getAmountTakingMails());
        System.out.println("Counter " + myPost2.counter);

    }
}




