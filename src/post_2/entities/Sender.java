package post_2.entities;

public class Sender implements Runnable {

    Post2 _post;
    private String name;

    public Sender(Post2 post, String name) {
        _post = post;
        this.name = name;
    }

    @Override
    public void run() {
        try {
            while (true) {
                _post.addMail("New letter");
                Thread.sleep((long) ((Math.random()) * 150));
            }
        } catch (InterruptedException ignored) {
            System.out.println(name + " sender finished");
        }
    }
}
