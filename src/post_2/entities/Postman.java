package post_2.entities;


public class Postman implements Runnable {

    Post2 _post;
    private String name;

    public Postman(Post2 post, String name) {
        _post = post;
        this.name = name;
    }

    @Override
    public void run() {
        try {
            while (true) {
                _post.takeMail();
                Thread.sleep(80);
            }
        } catch (InterruptedException ignored) {
            System.out.println(name + " postman finished");
        }
    }
}
