package post_2.entities;

import java.util.LinkedList;

public class Post2 {
    static Object keySender = new Object();
    static Object keyPostman = new Object();
    static private int _takingMails = 0;
    static int _sentMails = 0;
    private final int MAX_CAPACITY;
    public int counter = 0;
    private LinkedList<String> _mails;

    public Post2(int maxCapacity) {
        _mails = new LinkedList();
        MAX_CAPACITY = maxCapacity;
    }

    public boolean addMail(String newMail) {
        synchronized (keySender) {
            if (MAX_CAPACITY > _mails.size()) {
                _sentMails++;
                return _mails.add(newMail);
            }
        }
        return false;
    }

    public boolean takeMail() {
        synchronized (keyPostman) {
            if (0 < _mails.size()) {
                counter++;
                _mails.removeFirst();
                _takingMails++;
                return true;
            }
        }
        return false;
    }

    public int getAmountSentMails() {
        int result = _sentMails;
        return result;
    }

    public int getAmountTakingMails() {
        int result = _takingMails;
        return result;
    }

    public int getSize() {
        return _mails.size();
    }

    public int getMaxCapacity() {
        return MAX_CAPACITY;
    }

}
