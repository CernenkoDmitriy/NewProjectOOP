package javasummer2017;

import javasummer2017.entities.Passport;
import javasummer2017.entities.Visa;
import javasummer2017.sevices.PassportService;
import javasummer2017.sevices.accessors.PassportAccessor;
import javasummer2017.sevices.accessors.VisaAccessor;
import javasummer2017.sevices.accessors.impl.TextFilePassportAccessor;
import javasummer2017.sevices.accessors.impl.TextFilePassportService;
import javasummer2017.sevices.accessors.impl.TextFileVisaAccessor;

import java.util.*;

public class Main {

    public static void main(String[] args) {

//        testWritePassports();
//        testWriteVisas();
//        testReadPassports();
//        testReadAllPassports();
//        testReadVisas();
          testSavePassportServis();


//-------------------------------------------------------------
//        List list = new ArrayList();
//        list.add("");
//        list.clear();
//
//        if (list instanceof ArrayList) {
//            ArrayList arrayList = (ArrayList) list;
//            arrayList.ensureCapacity(5);
//        }

    }

    protected static void testReadPassports() {
        PassportAccessor passportAccessor = new TextFilePassportAccessor("passports.psp");
        Collection<Passport> passports = passportAccessor.readAll();
        System.out.println(passports);
    }

    protected static void testReadVisas() {
        VisaAccessor visaAccessor = new TextFileVisaAccessor("visas.psp");
        Collection<Visa> visas = visaAccessor.readAll();
        System.out.println(visas);
    }

    private static void testWriteVisas() {
        VisaAccessor visaAccessor = new TextFileVisaAccessor("visas.psp");
        Visa[] visas = {
                new Visa("AA213654",
                        "Gonduras",
                        new GregorianCalendar(2017, 11, 3),
                        new GregorianCalendar(2017, 12, 3)),

                new Visa("AA213654",
                        "Zimbabwe",
                        new GregorianCalendar(2017, 11, 3),
                        new GregorianCalendar(2019, 1, 1)),

                new Visa("AE154789",
                        "Zimbabwe",
                        new GregorianCalendar(2017, 11, 3),
                        new GregorianCalendar(2019, 1, 1)),

        };

        visaAccessor.save(Arrays.asList(visas));
    }

    private static void testWritePassports() {
        PassportAccessor passportAccessor = new TextFilePassportAccessor("passports.psp");
        Passport[] passports = {
                new Passport("Vasya", "Pupov", "AA213654"),
                new Passport("Petya", "Pupov", "AE154789"),
                new Passport("Grisha", "Pupov", "CA778877"),
                new Passport("Lisa", "Pupova", "KK998877")
        };

        passportAccessor.save(Arrays.asList(passports));
    }

    private static void testReadAllPassports() {
        PassportAccessor passportAccessor = new TextFilePassportAccessor("passports.psp");
        Collection<Passport> passports = passportAccessor.readAll();
        System.out.println(passports);

    }

    private static void testSavePassportServis() {
        VisaAccessor visaAccessor = new TextFileVisaAccessor("testPAV.psp");
        PassportAccessor passportAccessor = new TextFilePassportAccessor("testPAP.psp");
        Passport[] passports = {
                new Passport("Vasya", "Pupov", "AA213654"),
                new Passport("Petya", "Pupov", "AE154789"),
                new Passport("Grisha", "Pupov", "CA778877"),
                new Passport("Lisa", "Pupova", "KK998877")
        };
        passports[0].addVisa(new Visa("AA213654",
                "Gonduras",
                new GregorianCalendar(2017, 11, 3),
                new GregorianCalendar(2017, 12, 3)));
        passports[0].addVisa(new Visa("AA213654",
                "Zimbabwe",
                new GregorianCalendar(2014, 11, 3),
                new GregorianCalendar(2015, 12, 3)));
        passports[3].addVisa(new Visa("AA213654",
                "USA",
                new GregorianCalendar(2019, 11, 4),
                new GregorianCalendar(2019, 12, 5)));
        PassportService passportService = new TextFilePassportService(passportAccessor, visaAccessor);
        passportService.save(Arrays.asList(passports));
        System.out.println(passports);

    }
}
