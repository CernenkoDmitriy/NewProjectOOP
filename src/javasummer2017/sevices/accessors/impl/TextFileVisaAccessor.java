package javasummer2017.sevices.accessors.impl;

import javasummer2017.entities.Visa;
import javasummer2017.sevices.accessors.VisaAccessor;

import java.io.PrintStream;
import java.util.*;

public class TextFileVisaAccessor
        extends AbstractTextFileAccessor<Visa>
        implements VisaAccessor {


    public TextFileVisaAccessor(String fileName) {
        super(fileName);
    }

    @Override
    protected void saveEntity(PrintStream writer, Visa visa) {
        writer.println(visa.getPassportNumber());
        writer.println(visa.getCountry());
        writer.println(visa.getFrom().getTime().getTime());
        writer.println(visa.getTo().getTime().getTime());
    }

    @Override
    protected void readEntity(Collection<Visa> result, Scanner scanner) {
        String passportNumber = scanner.nextLine();
        String country = scanner.nextLine();

        String line = scanner.nextLine();
        Date readDataFrom = new Date ();
        readDataFrom.setTime(Long.parseLong(line));
        Calendar from = new GregorianCalendar ();
        from.setTime(readDataFrom);

        line = scanner.nextLine();
        Date readDataFromTo = new Date();
        readDataFromTo.setTime(Long.parseLong(line));
        Calendar to = new GregorianCalendar ();
        to.setTime(readDataFromTo);
        scanner.nextLine();
        Visa readVisa = new Visa(passportNumber, country, from, to);
        result.add(readVisa);
    }

    @Override
    public void startReading() {

    }

    @Override
    public Visa read() {
        return null;
    }

    @Override
    public void stopReading() {

    }
}
