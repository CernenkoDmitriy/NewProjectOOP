package javasummer2017.sevices.accessors.impl;

import javasummer2017.entities.Passport;
import javasummer2017.entities.Visa;
import javasummer2017.sevices.PassportService;
import javasummer2017.sevices.accessors.PassportAccessor;
import javasummer2017.sevices.accessors.VisaAccessor;

import java.util.ArrayList;
import java.util.Collection;

public class TextFilePassportService implements PassportService {
    private PassportAccessor _passportAccessor;
    private VisaAccessor _visaAccessor;

    public TextFilePassportService(PassportAccessor passportAccessor, VisaAccessor visaAccessor) {
        _passportAccessor = passportAccessor;
        _visaAccessor = visaAccessor;
    }

    @Override
    public Collection<Passport> readAll() {

        return null;
    }

    @Override
    public Collection<Passport> save(Collection<Passport> passports) {
        Collection<Visa> _tempCollectVisa = new ArrayList<>();
        for (Passport entity : passports) {
            _passportAccessor.save(passports);
            _tempCollectVisa.addAll(entity.getVisas());
        }
        _visaAccessor.save(_tempCollectVisa);
        return null;
    }
}
