package javasummer2017.sevices.accessors.impl;

import javasummer2017.entities.Passport;
import javasummer2017.sevices.accessors.PassportAccessor;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Scanner;

public class TextFilePassportAccessor
        extends AbstractTextFileAccessor<Passport>
        implements PassportAccessor {

    private String _fileName;

    public TextFilePassportAccessor(String fileName) {
        super(fileName);
        _fileName = fileName;
    }

    protected void saveEntity(PrintStream writer, Passport passport) {
        writer.println(passport.getNumber());
        writer.println(passport.getName());
        writer.println(passport.getLastName());
    }

    @Override

    protected void readEntity(Collection<Passport> result, Scanner scanner) {
        String number = scanner.nextLine();
        String firstName = scanner.nextLine();
        String lastName = scanner.nextLine();
        scanner.nextLine();
        Passport readPassport = new Passport(firstName, lastName, number);
        result.add(readPassport);
    }

    @Override
    public void startReading() {

    }

    @Override
    public Passport read() {
        return null;
    }

    @Override
    public void stopReading() {

    }
}
