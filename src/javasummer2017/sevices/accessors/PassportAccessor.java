package javasummer2017.sevices.accessors;

import javasummer2017.entities.Passport;

public interface PassportAccessor extends AbstractAccessor<Passport> {

}
