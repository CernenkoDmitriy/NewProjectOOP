package javasummer2017.sevices.accessors;

import javasummer2017.entities.Visa;

public interface VisaAccessor extends AbstractAccessor<Visa> {
}
