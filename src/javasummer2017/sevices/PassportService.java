package javasummer2017.sevices;

import javasummer2017.entities.Passport;

import java.util.Collection;

public interface PassportService {
    Collection<Passport> readAll();
    Collection<Passport> save(Collection<Passport> passports);

}
