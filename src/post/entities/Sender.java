package post.entities;

public class Sender implements Runnable {

    Post _post;

    public Sender(Post post) {
        _post = post;
    }

    @Override
    public void run() {
        try {
            while (true) {
                _post.addMail("New letter");
                Thread.sleep((long) ((Math.random()) * 150));
            }
        } catch (InterruptedException ignored) {
            System.out.println("End");
        }
    }
}
