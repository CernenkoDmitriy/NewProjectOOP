package post.entities;

public class Postman implements Runnable {

    Post _post;

    public Postman(Post post) {
        _post = post;
    }

    @Override
    public void run() {
        try {
            while (true) {
                _post.takeMail();
                Thread.sleep(80);
            }
        } catch (InterruptedException ignored) {
            System.out.println("End postman");
        }
    }
}
