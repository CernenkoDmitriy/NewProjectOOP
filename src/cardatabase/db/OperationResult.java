package cardatabase.db;

public enum OperationResult {
    SUCCESS,
    ERROR,
    EXIST,
    NOT_EXIST
}
