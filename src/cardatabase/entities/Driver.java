package cardatabase.entities;

import java.io.Serializable;

public class Driver implements Cloneable, Serializable {

    private String name;
    private String lastName;


    public Driver(String name, String lastName) {
        this.name = name;
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Driver clone (){
        try {
            Driver clone = (Driver) super.clone();
            return clone;
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

}
