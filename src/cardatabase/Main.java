package cardatabase;

import cardatabase.db.DataBase;
import cardatabase.db.impl.memory.MemoryDataBase;
import cardatabase.entities.Car;
import cardatabase.entities.CarNumber;

import java.awt.*;
import java.io.*;
import java.util.GregorianCalendar;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        DataBase dataBase = getDataBase();
       /* Car car = new Car("Mersedes", "1200", new GregorianCalendar(2010, 2, 2), Color.BLACK);
        Car car1 = new Car("BMW", "600", new GregorianCalendar(2011, 2, 2), Color.BLACK);
        Car car2 = new Car("Jiguli", "2001", new GregorianCalendar(1960, 2, 2), Color.BLACK);
        dataBase.add(new CarNumber("AA", 2546, "BB"), car);

        System.out.println(dataBase.delete(car).name());
        System.out.println(dataBase.delete(car).name());
        //fill

        CarNumber carNumber = new CarNumber("AA", 2546, "BB");
//        Car car = dataBase.findByCarNumber(carNumber);
        car.setColor(Color.BLACK);*/

        // getDataBase - tested add
        // testDeleteByCar();
        //testDeleteByCarNumber();
        //testFindByCarNumber();
        //testFindAll();
        //testDeleteAll();
        serializeDataBase();

    }

    public static DataBase getDataBase() {
        DataBase dataBase = new MemoryDataBase();
        Car car = new Car("Mersedes", "1200", new GregorianCalendar(2010, 2, 2), Color.BLACK);
        Car car1 = new Car("BMW", "600", new GregorianCalendar(2011, 2, 2), Color.BLACK);
        Car car2 = new Car("Jiguli", "2001", new GregorianCalendar(1960, 2, 2), Color.BLACK);
        dataBase.add(new CarNumber("AS", 2546, "BB"), car);
        dataBase.add(new CarNumber("AA", 1234, "DS"), car1);
        dataBase.add(new CarNumber("AD", 7854, "CB"), car2);
        return dataBase;
    }

    public static void testDeleteByCar() {
        DataBase dataBase = getDataBase();
        Car car = new Car("Mersedes", "1200", new GregorianCalendar(2010, 2, 2), Color.BLACK);
        Car car1 = new Car("BMW", "600", new GregorianCalendar(2011, 2, 2), Color.BLACK);
        Car car2 = new Car("Jiguli", "2001", new GregorianCalendar(1960, 2, 2), Color.BLACK);
        System.out.println(dataBase.delete(car).name());
        System.out.println(dataBase.delete(car1).name());
        System.out.println(dataBase.delete(car2).name());
        System.out.println(dataBase.delete(car).name());
        System.out.println("Done testDeleteByCar");
    }

    public static void testDeleteByCarNumber() {
        DataBase dataBase = getDataBase();
        CarNumber carNumber = new CarNumber("AS", 2546, "BB");
        CarNumber carNumber1 = new CarNumber("AA", 1234, "DS");
        CarNumber carNumber2 = new CarNumber("AD", 7854, "CB");
        System.out.println(dataBase.delete(carNumber).name());
        System.out.println(dataBase.delete(carNumber1).name());
        System.out.println(dataBase.delete(carNumber2).name());
        System.out.println(dataBase.delete(carNumber).name());
        System.out.println("Done testDeleteByCarNumber");
    }

    public static void testFindByCarNumber() {
        DataBase dataBase = getDataBase();
        CarNumber carNumber = new CarNumber("AS", 2546, "BB");
        System.out.println(dataBase.findByCarNumber(carNumber));
        System.out.println("Done testFindByCarNumber");
    }

    public static void testFindAll() {
        DataBase dataBase = getDataBase();
        Map<CarNumber, Car> secondDataBase = dataBase.findAll();
        secondDataBase.forEach((CarNumber, Car)-> System.out.println(Car.getMark()));
        System.out.println("Done testFindAll");
    }

    public static void testDeleteAll (){
        DataBase dataBase = getDataBase();
        System.out.println(dataBase.deleteAll().name());
        Map<CarNumber, Car> secondDataBase = dataBase.findAll();
        secondDataBase.forEach((CarNumber, Car)-> System.out.println(Car.getMark()));
        System.out.println("Done testDeleteAll");
    }
    public static void serializeDataBase (){
        DataBase dataBase = getDataBase();
        System.out.println(dataBase);
         try (ObjectOutputStream oos =
                     new ObjectOutputStream(new FileOutputStream("serilizate.psp"));) {
           oos.writeObject(dataBase);
        } catch (IOException e) {
             e.printStackTrace();
         }

        System.out.println("Result");
        try (ObjectInputStream ois = new ObjectInputStream (new FileInputStream("serilizate.psp"))) {
            DataBase readDataBase  = (DataBase) ois.readObject();
            System.out.println(readDataBase);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
